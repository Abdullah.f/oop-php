<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OOP PHP</title>
</head>
<body>
    <h2> OOP PHP </h2>

    <?php
    /* Release 0 */
    require 'Animals.php';
    require 'Ape.php';
    require 'Frog.php';

    /* Release 1 */
    $sheep = new Animal("shaun");
    echo "Name : ".$sheep->name."<br>"; // "shaun"
    echo "Legs : ".$sheep->legs."<br>"; // 4
    echo "Cold Blooded : ".$sheep->cold_blooded."<br><br>"; // "no"

    $sungokong = new Ape("kera sakti");
    echo "Name : ".$sungokong->name."<br>"; // "kera sakti"
    echo "Legs : ".$sungokong->legs."<br>"; // 2
    echo "Cold Blooded : ".$sungokong->cold_blooded."<br>"; // "no"
    echo "Yell : ";
    echo $sungokong->yell()."<br><br>"; // "Auooo"

    $kodok = new Frog("buduk");
    echo "Name : ".$kodok->name."<br>"; // "buduk"
    echo "Legs : ".$kodok->legs."<br>"; // 4
    echo "Cold Blooded : ".$kodok->cold_blooded."<br>"; // "no"
    echo "Sound : ";
    echo $kodok->jump()."<br>"; // "Hop Hop"

    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
    ?>
</html>