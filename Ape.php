<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OOP PHP</title>
</head>
<body>
    <?php
    /* Release 0 */
    require_once 'Animals.php';
    /* Class Ape */
    class Ape extends Animal {
        public function yell() {
            echo "Auooo";
        }
        public $legs = 2;
    }
    ?>
</body>
</html>